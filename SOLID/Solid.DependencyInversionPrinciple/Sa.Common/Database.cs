﻿using System.Data;
using System.Reflection;

public static class DataReaderExtensions
{
	public static bool FieldExists(this IDataReader dataReader, string columnName)
	{
		dataReader.GetSchemaTable().DefaultView.RowFilter = string.Format("ColumnName= '{0}'", columnName);
		return (dataReader.GetSchemaTable().DefaultView.Count > 0);
	}
}

namespace Sa
{
	public class Database
	{
		public static string dbConnectionString(string dbInst, string dbName, string UserName, string UserPass)
		{
			string cn = "Data Source=" + dbInst + ";";
			cn = cn + "Initial Catalog=" + dbName + ";";
			cn = cn + "User Id=" + UserName + ";";
			cn = cn + "Password=" + UserPass + ";";

			cn = cn + "Application Name = " + ProdNameVer() + ";";

			return cn;
		}

		public static string ProdNameVer()
		{
			Assembly ai = Assembly.GetExecutingAssembly();
			return ai.GetName().Name + " (" + ai.GetName().Version + ")";
		}
	}
}